<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       return [
           'title'  =>  $this->title,
           'slug'   =>  $this->slug,
           'path'  =>  $this->path,
           'description'    =>  $this->description,
           'created_at'  =>  $this->created_at->diffForHumans(),
           'user_id'    =>  $this->user_id,
       ];
    }
}
