<?php

namespace App\Model;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['title', 'slug', 'description', 'user_id'];
    

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($task) {
            $task->slug = str_slug($task->title);
            $task->user_id = auth()->id();
        });
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getPathAttribute()
    {
        return "/task/$this->slug";
    }
}
