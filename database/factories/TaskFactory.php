<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Model\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    $title = $faker->sentence;
    return [
        'title' => $title,
        'slug'  =>  str_slug($title),
        'description'   => $faker->text,
        'user_id'   =>  function() {
            return User::all()->random();
        }
    ];
});
