import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);
import Login from './../components/login/login';
import Sigup from './../components/login/Signup';
import Logout from './../components/login/Logout';
import Tasks from './../components/task/Tasks';
import Create from './../components/task/Create';
import Read from './../components/task/Read';

const routes = [
     {path: '/login', component: Login},
     {path: '/signup', component: Sigup},
     {path: '/task', component: Tasks},
     {path: '/task/:slug', component: Read, name: 'read'},
     {path: '/logout', component: Logout},
     {path: '/create', component: Create},
]

const router = new VueRouter({
    mode: 'history',
    routes
});  

export default router